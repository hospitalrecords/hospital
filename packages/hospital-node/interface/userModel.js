let patient = {
    date :{
        type: String,
        trim: true,
        required: true
    },
    name: {
        type: String,
        trim: true,
        required: true
    },
    gender :{
        type: String,
        trim: true,
        required: true
    },
    age : {
        type: String,
        trim: true,
        required: true
    },
    mobile :{
        type: String,
        trim: true,
        required: true
    },
    address:{
        type: String,
        trim: true,
        required: true
    },
    department:{
        type: String,
        trim: true,
        required: true
    },
    doctorname:{
        type: String,
        trim: true,
        required: true
    },
    email:{
        type: String,
        trim: true,
        required: true
    },
   
}
module.exports = patient