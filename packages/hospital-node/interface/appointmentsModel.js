let appointments = {
    date :{
        type: String,
        trim: true,
        required: true
    },
    id :{
        type: String,
        trim: true,
        required: true
    },
    department :{
        type: String,
        trim: true,
        required: true
    },
    doctorname :{
        type: String,
        trim: true,
        required: true
    }
   
}
module.exports = appointments;