const mongoose  = require("mongoose");
const DOCTORSCHEMA = require('../interface/doctorModel');
const CONFIG = require('../config/config');

let headDoctor = {
    postDoctors : postDoctors,
    getDoctors :getDoctors
}

var doctors = mongoose.model('doctors', DOCTORSCHEMA);

async function postDoctors (req, res)
{
let doctor = req.body;

var newDoctor = new doctors(doctor);

await newDoctor.save((err, datas)=> {
    if(err) 
    {
       return res.status(500).send(err);     
    }   
    else
    {
        return res.status(200).send("A New Doctor Added Successfully");
    }   
})
}

async function getDoctors(req, res){   
    await doctors.find((err, doctorsList) => {          
            if (err) {        
                return res.status(500).send(err);
            }
           else{
            return res.status(200).send(doctorsList);
           }
        });

}/// fn to get doctors from doctors collection


module.exports = headDoctor; 