const MONGOOSE = require('mongoose');
const USERSCHEMA = require('../interface/userModel');
const CONFIG = require('../config/config');

let model = {
    appointments : appointments,
    getPatientsData : getPatientsData,
    deletePatients : deletePatients
}

// create a schema for the user.
var patient = MONGOOSE.model('patient',USERSCHEMA);

async function appointments(req, res  ){
    let patientData = req.body;   
    // convert the schema to a mongo model
    var newPatient = new patient(patientData);
   
    // save the document to mongodb      
          
        await newPatient.save((err)=> {
            if(err) 
            {
               return res.status(500).send(err);     
            }   
            else
            {
                return res.status(200).send("A New Appointment Recorded Successfully");
            }   
           
           
        });
  
    }
// fn to store appointments


async function getPatientsData(req, res){   
        await patient.find({}, function (err, patientsDetails)  {          
                if (err) {        
                    return res.status(500).send(err);
                }
               else{
                return res.status(200).send(patientsDetails);
               }
            });
   
}/// fn to get patients from appointments

async function deletePatients(id, res){   
    
    await patient.findOneAndDelete({ "_id" : id }, function (err, deletedPatients)  {
                  
            if (err) {        
                return res.status(500).send(err);
            }
           else{
            return res.status(200).send("Patient Deleted Successfully");
           }
        });

}/// fn to delete patients 

module.exports = model;