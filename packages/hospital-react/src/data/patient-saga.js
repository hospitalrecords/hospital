import {call, put, all, takeLatest, takeEvery} from 'redux-saga/effects';
import Actions from '../data/patient-action';
import {appointments, getPatients, removePatients, bookDoctorAppointment} from '../services/appointmentsApi'; 
import {postDepartments, getDepartments} from '../services/departments';
import {postDoctors, getDoctors} from '../services/doctors';
import patientModel from "../models/patientsModel";
import departmentModel from "../models/department";
import DoctorsModel from "../models/doctors";


function* _getAppointments(action)

{
    //console.log("saga",payload);
    const appointmentslist = yield call (appointments, action.payload);
    yield put ( {type:Actions.GET_APPOINTMENTS, payload: appointmentslist});
}

function* _getPatients()

{
    try
    {
    const getPatient = yield call (getPatients);
    
    const patientDetails = [];
    getPatient.data.map(patient => {
        const patientsModel = new patientModel(patient);
        patientDetails.push(patientsModel);
    });    
     yield put ( {type:Actions.GET_PATIENTS, payload: patientDetails});
     
    }
    catch(error)
    {
        console.log("Failed to get patients data", error)
    }
}

function* _postDepartments(action)
{
    try
    {
        const department = yield call(postDepartments, action);
       yield put({type: Actions.SET_DEPARTMENTS, payload: department});
    }
    catch(error)
    {
        console.log("Error in Strong Departments", error)
    }
}

function* _getDepartments(action)
{
    try
    {
        const departmentsList = yield call(getDepartments);

        const departments = [];
        departmentsList.data.map(dept => {
        const departmentsModel = new departmentModel(dept);
        departments.push(departmentsModel);
    });  
       yield put({type: Actions.GET_DEPARTMENTS_LIST, payload: departments});
    }
    catch(error)
    {
        console.log("Error in Strong Departments", error)
    }
}

function* _postDoctors(action)
{
    try
    {
        const department = yield call(postDoctors, action);
        console.log(department);
    //    yield put({type: Actions.SET_DEPARTMENTS, payload: department});
    }
    catch(error)
    {
        console.log("Error in Strong Departments", error)
    }
}

function* _getDoctors(action)
{
    try
    {
        const doctorsList = yield call(getDoctors);
        const doctors = [];
        doctorsList.data.map(doctor => {       
        const doctorsModel = new DoctorsModel(doctor);
        doctors.push(doctorsModel);
    });  
       yield put({type: Actions.DOCTORS_LIST, payload: doctors});
    }
    catch(error)
    {
        console.log("Error in Strong Departments", error)
    }
}

function* _deletePatients(action)
{
    try
    {
        const deletePatients = yield call(removePatients, action);
        
        yield put({type: Actions.GET_DELETE_PATIENTS, payload: deletePatients});
    }
    catch(error)
    {
        console.log("Error in Deleting Patients", error)
    }
}

function* _bookAppointments(action){
    try
    {
        const bookAppointment = yield call(bookDoctorAppointment, action);
        yield put({type: Actions.GET_BOOK_APPOINTMENTS, payload: bookAppointment});
    }
    catch(error)
    {
        console.log(error);
    }
}
function* actionWatcher() {
    yield all([takeLatest(Actions.POST_APPOINTMENTS, _getAppointments)]);
    yield all([takeLatest(Actions.POST_PATIENTS, _getPatients)]);
    yield all([takeLatest(Actions.POST_DEPARTMENTS, _postDepartments)]);
    yield all([takeLatest(Actions.DISPLAY_DEPARTMENTS, _getDepartments)]);
    yield all([takeLatest(Actions.POST_DOCTORS, _postDoctors)]);
    yield all([takeLatest(Actions.DISPLAY_DOCTORS, _getDoctors)]);
    yield all([takeLatest(Actions.DELETE_PATIENTS, _deletePatients)]);
    yield all([takeLatest(Actions.SET_BOOK_APPOINTMENTS, _bookAppointments)]);
}
   

export default function* patientsSaga() {
    yield all([actionWatcher()]);
}
