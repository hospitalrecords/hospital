import React from 'react';
import {useHistory} from 'react-router-dom';
import { FaChevronCircleLeft } from 'react-icons/fa';
import '../../styles/backbutton.sass';

function Back (props)
{
const history =useHistory();
    const _handelClick=(event)=>{
        event.preventDefault();
        history.goBack();
           }
    return(<div className="backButton" onClick={_handelClick} ><FaChevronCircleLeft/></div>);
        
}

export default Back;