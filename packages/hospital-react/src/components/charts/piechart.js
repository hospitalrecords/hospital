import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Actions from '../../data/patient-action';
import { Bar , Line, Pie,Doughnut  } from 'react-chartjs-2';



function PatientsByDepartment () {
    const departmentList = useSelector(state => state.PatientsData.departmentsList);
    const patients = useSelector(state => state.PatientsData.patients);
    const doctorsList = useSelector(state => state.PatientsData.doctorsList);
    const dispatch = useDispatch();


        useEffect(() => {
        dispatch({ type: Actions.DISPLAY_DEPARTMENTS });
        dispatch({ type: Actions.DISPLAY_DOCTORS });
        dispatch({ type: Actions.POST_PATIENTS });
    }, [])



const patientsPerDepartment =()=>{
    return doctorsList.map(({doctorname}) => patients.reduce(function(accumulator, patient) {
        if(patient.doctorname === doctorname) {
            accumulator++;
        }            
        return accumulator;
    }, 0));
}

const data = {
    labels: doctorsList.map(doctor => doctor.doctorname),        
    datasets: [{
        label: "Doctors",
        backgroundColor: [
            '#fb3b01',
            '#a8f3f3',
            '#0069d9',
            '#ffc107',
            '#5f9276'
        ],
        borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)'
            
        ],
        data: patientsPerDepartment(),
    }]
}

    return (
        <div>
            <Doughnut  data={data}/>
        </div>
    )
}

export default PatientsByDepartment;