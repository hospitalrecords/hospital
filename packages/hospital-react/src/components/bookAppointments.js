import React, { useState, useEffect, useForm } from 'react';
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import Input from './forms/input';
import Actions from '../data/patient-action';
import { Form, Dropdown, Card, FormGroup, Container, Row, Col, Button, Modal } from "react-bootstrap";
import Buttons from './forms/button';
import "../styles/appointments.sass";
import { FaListOl } from 'react-icons/fa';
import { $ } from "jquery";
import formInputValidations from './validations/validation';

function Appointments({patientData}) {
    
    const [patientDetails, setPatientDetails] = useState({});
    const [modalOpenClose, setModalOpenClose] = useState(false);
    const [department, setDepartment] = useState();
    const [filterDoctor, setFilterDoctor] = useState();
    const dispatch = useDispatch();
    const history = useHistory();
    const appointmentsResponse = useSelector(state => state.PatientsData.appointments);
    const departments = useSelector(state => state.PatientsData.departmentsList);
    const doctors = useSelector(state => state.PatientsData.doctorsList);
    const appoitmentNotify = useSelector(state => state.PatientsData.bookedAppointmentsNotify);
    
       useEffect(() => {
        //To get the present date onloading of page
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        today = mm + '/' + dd + '/' + yyyy;

        setPatientDetails({ ...patientDetails, date: today });
        dispatch({ type: Actions.DISPLAY_DEPARTMENTS });
        dispatch({ type: Actions.DISPLAY_DOCTORS });
    
    }, []);

    const onFormSubmit = (e) => {
        e.preventDefault();
        // dispatch({ type: Actions.POST_APPOINTMENTS, payload: patientDetails });
        setModalOpenClose(!modalOpenClose);
        
        dispatch({type: Actions.SET_BOOK_APPOINTMENTS, payload: patientDetails });
    }

    const onChange = (target) => {

        const { name, value } = target;   
        const patientId =  patientData.id
        setPatientDetails({ ...patientDetails, [name]: value, department: department, id: patientId });
       
    }
    const _handleClose = () => {
        setModalOpenClose(!modalOpenClose);
        
    }

    const AlertComponent = () => {
        return (
            <Modal show={modalOpenClose} onHide={_handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Appointments</Modal.Title>
                </Modal.Header>
                <Modal.Body>{appoitmentNotify.data}</Modal.Body>
            </Modal>
        )
    }
   
    const _handleDepartmentChange = (e) => {
        e.preventDefault();
        setDepartment(e.target.value);
        setFilterDoctor(doctors.filter(({ doctordepartment }) => doctordepartment === e.target.value));

    }
    

    return (
        <div >
            {modalOpenClose && <AlertComponent />}
            <Form onSubmit={onFormSubmit} id="create-course-form">
                <FormGroup role="form">
                    <Input type="text" placeholder="Enter Todays Date" value={patientDetails.date} fieldname="Date" disabled />
                    <Input type="text" placeholder="Patient ID" name="id" value={patientData.id} fieldname="Patient ID"  onChange={e => onChange(e.target)}/>
                    <label>Choose the Consult Department</label>
                    <select className="ui dropdown fluid" name="department" onChange={_handleDepartmentChange} value={department}>
                        <option value="0">Select Department</option>
                        {departments.map((dept, index) => {
                            return (
                                <option key={index} value={dept.department}>
                                    {dept.department} - {dept.description}
                                </option>
                            );
                        })}
                    </select>
                    
                    <label>Choose the Consultant Doctor</label>
                    <select className="ui dropdown fluid" name="doctorname" onChange={e => onChange(e.target)} value={patientDetails.consultdoctor}>
                        <option value="0">Select Doctor to Consult</option>
                        {filterDoctor && filterDoctor.map((doctor, index) => {
                            return (
                                <option key={index} value={doctor.doctorname}>
                                    {doctor.doctorname}
                                </option>
                            );
                        })}
                    </select>
                                  
                </FormGroup>
                <Buttons name="Book Appointment" type="submit" onClick={onFormSubmit} />
            </Form>


        </div>

    )
}

export default Appointments;
;