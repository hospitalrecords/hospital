
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Row, Col, Form, ListGroup, Carousel, InputGroup, FormControl, Card } from 'react-bootstrap';
import Actions from '../data/patient-action';
import BackButton from './backbutton/back';
import Modal from './forms/modal';
import Input from './forms/input';
import { FaSearch } from 'react-icons/fa';
import '../styles/doctors.sass';
import Search from './forms/search';
import Table from './forms/table';

function Doctors() {
    const [filteredDocotor, setFilteredDoctor] = useState([]);
    const [doctorSearch, setDoctorSearch] = useState();
    const [consultedPatients, setConsultedPatients] = useState([]);
    const dispatch = useDispatch();
    const doctorsList = useSelector(state => state.PatientsData.doctorsList);
    const patients = useSelector(state => state.PatientsData.patients);

    useEffect(() => {
        dispatch({ type: Actions.DISPLAY_DEPARTMENTS });
        dispatch({ type: Actions.DISPLAY_DOCTORS });
        dispatch({ type: Actions.POST_PATIENTS });

    }, []);

    const _searcedDoctorData = (searchedDoctor) => {
        return (
            searchedDoctor.map((doctorsData, index) => {
                return (
                    <>
                        <Card.Header className="summaryHeader" as="h4">Doctor's Profile</Card.Header>
                        <ListGroup key={doctorsData.id}>
                            <ListGroup.Item><span className="doctorProfile"> Name:</span> {doctorsData.doctorname}</ListGroup.Item>
                            <ListGroup.Item><span className="doctorProfile"> Department:</span> {doctorsData.doctordepartment}</ListGroup.Item>
                            <ListGroup.Item><span className="doctorProfile"> Mobile: </span> {doctorsData.mobile}</ListGroup.Item>
                            <ListGroup.Item><span className="doctorProfile"> Email:</span> {doctorsData.email}</ListGroup.Item>
                        </ListGroup>
                    </>
                )
            })
        )
    }

    const _tableHead = () => {
        return (
            <>
                <th>#</th>
                <th>Name</th>
                <th>Date</th>
                
            </>
        )
    }

    const _consultedPatients = (consultedPatients) => {
        return (           
            consultedPatients.map((patientData, index) => {
                return (
                    <>
                         <tr key={index}>
                        <td>{index +1}</td><td>{patientData.name}</td><td>{patientData.date}</td>
                        </tr>
                    </>
                )
            })
        ) 
    }

    const _handleDoctorSearch = (e) => {
        e.preventDefault();
        setDoctorSearch(e.target.value)

        const searchedDoctor = doctorsList.filter(doctor => {

            if (doctor.doctorname === doctorSearch) {
                return true
            }
            else {
                return false
            }
        });

       //Patients visited this particular doctor
        const consultedPatient = patients.filter(patient => {

            if (patient.doctorname === doctorSearch) {
                return true
            }
            else {
                return false
            }
        });


        setFilteredDoctor(searchedDoctor);
        setConsultedPatients(consultedPatient)
    }

    const _handleDoctorsChange = (e) => {
        setDoctorSearch(e.target.value)
    }
    return (<div className="doctorsPage">
        <BackButton className="" />
        <div className="content">
            <Search name="doctorsearch" placeholder="Type Doctor to Search..." onChange={_handleDoctorsChange} value={doctorSearch} onClick={_handleDoctorSearch} />
            <br />
            <Row>
                <Col xs={4}>
                    {filteredDocotor.length > 0 && _searcedDoctorData(filteredDocotor)}
                </Col>
                <Col xs={8}>
                <Card.Header className="summaryHeader" as="h4">Patients Consulted This Doctor So Far...</Card.Header> 
                    {consultedPatients.length > 0 ?  <Table thead={_tableHead()} tbody={_consultedPatients(consultedPatients)} className="patientsTable"/> : "No Records"}
                </Col>
            </Row>
        </div>
    </div>
    )
}

export default Doctors;
