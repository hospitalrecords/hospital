import React, { useEffect, useState } from 'react';

import Input from './forms/input';
import Buttons from './forms/button';
import {Form, FormGroup} from 'react-bootstrap';

function EditableForm ({patients, onChange}) {    

    const [patientDetails, setPatientDetails] = useState({
        name: patients.name,
        age: patients.age,
        mobile :patients.mobile,
        address: patients.address,
        email: patients.email
    });

    const _OnchnageInputs = (target) => {        
         const { name, value } = target;            
         setPatientDetails({...patientDetails, [name]: value});
         console.log("pd", patientDetails);
    }

    const onFormSubmit=()=>
    {

    }
    return (        
           
        <Form onSubmit={onFormSubmit} id="create-course-form">
            <FormGroup role="form">                  
                <Input type="text" name="name" value={patientDetails.name} fieldname="Patient Name" onChange={e => _OnchnageInputs(e.target)} />
                <Input type="text" name="age" value={patientDetails.age} fieldname="Patient age" onChange={e => _OnchnageInputs(e.target)} />
                <Input type="text" name="mobile" value={patientDetails.mobile} fieldname="Mobile Number" onChange={e => _OnchnageInputs(e.target)} />
                <Input type="text" name="address" value={patientDetails.address} fieldname="Patient Address" onChange={e => _OnchnageInputs(e.target)} />
                <Input type="text" name="email" value={patientDetails.email} fieldname="Patient Email" onChange={e => _OnchnageInputs(e.target)} />           
                <Buttons name="Update" type="submit" />                
            </FormGroup>
        </Form>
    )
}


export default EditableForm;