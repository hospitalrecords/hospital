import React from 'react';
import {ButtonToolbar, Button} from "react-bootstrap"; 

function Buttons (props)
{
    return (
    <div>
        
    <ButtonToolbar>
    <Button variant="primary" type={props.type} onClick={props.onClick} size="md" block >{props.name}</Button>
  </ButtonToolbar>
     </div>
     )
}

export default Buttons;