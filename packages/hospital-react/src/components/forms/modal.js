import React from 'react';
import {Modal} from 'react-bootstrap';

function Modals (props)
{
  console.log(props);
    return (<div>
        <Modal
        {...props}
        size="md"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {props.heading}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>        
      
            {props.content}
          
        </Modal.Body>
        
      </Modal></div>)
}

export default Modals;