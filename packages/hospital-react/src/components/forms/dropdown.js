import React from 'react';
import {Dropdown} from 'react-bootstrap';

function DropDowns (props)  {
    return (
        <Dropdown onChange={props.onChange}>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
                {props.toggle}
            </Dropdown.Toggle>

            <Dropdown.Menu>
                {props.dropDownData.map((item, index) => 
                <Dropdown.Item  key={index} value={item.value}>{item.name}</Dropdown.Item>)}
            </Dropdown.Menu>
        </Dropdown>
    )
}
export default DropDowns;

                