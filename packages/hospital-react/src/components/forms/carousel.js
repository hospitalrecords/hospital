import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FaWheelchair, FaDollarSign} from 'react-icons/fa';
import {Carousel, Row, Col } from 'react-bootstrap';
import PatientsVisit from "../../assets/images/patientsvisit3.jpg";
import RevenueSummary from "../../assets/images/revenue2.jpg";
import Actions from '../../data/patient-action';

function Carousels()
{
  const dispatch = useDispatch();
  const patients  = useSelector(state => state.PatientsData.patients);
  const [index, setIndex] = useState(0);
      
        const handleSelect = (selectedIndex, e) => {
          setIndex(selectedIndex);
        };
      
        useEffect(() => {
          dispatch({ type: Actions.DISPLAY_DOCTORS });
          dispatch({type: Actions.POST_PATIENTS});
      }, [])

        return (
          <Carousel activeIndex={index} onSelect={handleSelect}>
            <Carousel.Item className="ab">
              <img
                className="d-block w-100"
                src={PatientsVisit}
                alt="First slide"
              />
              <Carousel.Caption>
                <p className="carouselHeader"><FaWheelchair/> Patients Visited Summary</p>         
                <Row><Col xs={8}><p className="counts"> No.of Patients Visited So Far is</p></Col><Col xs={4} className="numbers">{patients.length}</Col></Row>
              </Carousel.Caption>
            </Carousel.Item>
             <Carousel.Item>
              <img
                className="d-block w-100"
                src={RevenueSummary}
                alt="Second slide"
              />
               <Carousel.Caption>
                <p className="carouselHeader"><FaDollarSign/> Revenue Summary</p>
                <Row><Col xs={8}><p className="counts">Amount Collected So Far is</p></Col><Col xs={4} className="numbers">$ 20</Col></Row>
              </Carousel.Caption> 
            </Carousel.Item>     
          </Carousel>
        );
      
}

export default Carousels;