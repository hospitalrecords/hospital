import React from 'react';
import {Table} from "react-bootstrap";

function Tables(props)
{
    return (
<>
    <Table responsive bordered hover size="sm" className={props.className}>
        <thead>
            <tr>
            {props.thead}
            </tr>
        </thead>
        <tbody>            
            {props.tbody}              
        </tbody>
    </Table>
</>
    )
}

export default Tables;