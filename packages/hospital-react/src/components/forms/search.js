import React from 'react';


function Search(props) {

    return (
        <>
            <form className="ui form">
                <div className="ui action input">
                    <input type="text" placeholder={props.placeholder} name={props.name} value={props.value} onChange={props.onChange} />
                    <button className="ui icon button"><i aria-hidden="true" className="search icon" onClick={props.onClick}></i></button>
                </div>

            </form>
        </>
    )
}

export default Search;