import Axios from 'axios';

export const postDoctors = async (action)=>
{
    try{
    const doctors = await Axios.post("http://3.7.92.192:3001/api/postdoctors", action.payload);

    console.log(doctors);
    }
    catch(error)
    {
        console.log(error);
    }
}

export const getDoctors = async (action)=>
{
    try{
    const doctorsList = await Axios.get("http://3.7.92.192:3001/api/getdoctors");

    return doctorsList;
    }
    catch(error)
    {
        console.log(error);
    }
}