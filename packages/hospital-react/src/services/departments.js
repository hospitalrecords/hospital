import Axios from "axios";


export const postDepartments = async(action)=>
{
    try {
      
        const departments = await Axios.post("http://3.7.92.192:3001/api/addingdepartments", action.payload);   
        
                return departments;  
                 
    }
    catch (err) {
        console.log(err);
    }
}

export const getDepartments = async(action)=>
{
    try {
        
        const departmentsList = await Axios.get("http://3.7.92.192:3001/api/getdepartments");   
        
                return departmentsList;  
                 
    }
    catch (err) {
        console.log(err);
    }
}