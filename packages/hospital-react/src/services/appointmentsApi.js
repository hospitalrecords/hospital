import Axios from "axios";

export const appointments = async (action) => {

    try {
        const appointmentsData = await Axios.post("http://3.7.92.192:3001/api/appointments", action);
        return appointmentsData;
    }
    catch (err) {
        console.log("Unable to Store the appointments", err)
    }
}


export const getPatients = async (action) => {
    try {
        const patientsList = await Axios.get("http://3.7.92.192:3001/api/getPatients");

        return patientsList;
       
    }
    catch (err) {
        console.log(err)
    }
}

export const removePatients = async (action) => {
    try {
        let id = action.payload;
        console.log("id", id);
        const deletePatient = await Axios.post(`http://3.7.92.192:3001/api/deletePatients/${id}`);

        return deletePatient;
       
    }
    catch (err) {
        console.log(err);
    }
}

export const bookDoctorAppointment = async (action) => {
    try {
        
        console.log("api",action.payload.id);

        const id = action.payload.id;

        const bookAppointment = await Axios.post(`http://3.7.92.192:3001/api/bookAppointment`, action);

        return bookAppointment;
       
    }
    catch (err) {
        console.log(err);
    }
}

