import React from 'react';
import { BrowserRouter as Router, Switch, Link, Route, Redirect,NavLink } from 'react-router-dom';
import Dashboard from '../components/dashboard';
import Appointments from '../components/appointments';
import Patients from '../components/patients';
import Doctors from '../components/doctors';
import Payments from '../components/payments';
import { Navbar, Nav, Container } from "react-bootstrap";
import { FaClinicMedical, FaMicrosoft, FaTasks, FaWheelchair, FaDollarSign, FaStethoscope } from 'react-icons/fa';
import * as Constants from '../utils/constants';
import App from '../components/app';
import '../styles/navbar.sass';

function Routing() {
    return (
        <div>
            
                <Router basename="hospital">
                    <Navbar bg="dark" variant="dark">
                        <NavLink to="/home" activeStyle={{fontWeight: "bold", color: "red", backgroundColor:"white"}} className="navname"><FaClinicMedical/> {Constants.Hospital}</NavLink>
                        <Nav className="mr-auto">
                            <NavLink to="/dashboard" activeStyle={{fontWeight: "bold", color: "red", backgroundColor:"white"}} className="navname"><FaMicrosoft/> {Constants.Dashboard}</NavLink>
                            <NavLink to="/appointments" activeStyle={{fontWeight: "bold", color: "red", backgroundColor:"white"}} className="navname"><FaTasks/> {Constants.Appointments}</NavLink>
                            <NavLink to="/patients"  activeStyle={{fontWeight: "bold", color: "red", backgroundColor:"white"}} className="navname"> <FaWheelchair/> {Constants.Patients}</NavLink>
                            <NavLink to="/doctors" activeStyle={{fontWeight: "bold", color: "red", backgroundColor:"white"}} className="navname"><FaStethoscope/> {Constants.Doctors} </NavLink>
                            <NavLink to="/payments" activeStyle={{fontWeight: "bold", color: "red", backgroundColor:"white"}} className="navname"><FaDollarSign/> {Constants.Payments}</NavLink>
                        </Nav>
                    </Navbar>

                    <Switch>
                        <Redirect exact from="/" to="home"/> 
                        <Route exact path="/home" component={App}/>
                        <Route exact path="/" component={App}></Route>
                        <Route exact path="/dashboard" component={Dashboard}></Route>
                        <Route exact path="/appointments" component={Appointments}></Route>
                        <Route exact path="/patients" component={Patients}></Route>
                        <Route exact path="/doctors" component={Doctors}></Route>
                        <Route exact path="/payments" component={Payments}></Route>
                    </Switch>
                </Router>
           
        </div>
    )

}

export default Routing;
