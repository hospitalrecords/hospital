import { combineReducers } from "redux";
import PatientsData from "../data/patient-reducer";


const rootHospitalStoreReducer = combineReducers({
    PatientsData,
   
});
export default rootHospitalStoreReducer;
