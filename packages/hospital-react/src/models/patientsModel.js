class patientModel {
    constructor(patientsData = {}) {
        this.id = patientsData._id || "";
        this.date = patientsData.date || "";
        this.name = patientsData.name || "";
        this.gender = patientsData.gender || "";
        this.age = patientsData.age || "";
        this.mobile = patientsData.mobile || "";
        this.address = patientsData.address || "";
        this.department = patientsData.department || "";
        this.doctorname = patientsData.doctorname || "";
        this.email = patientsData.email || "";
        
       
    }
}

export default patientModel;