class department {
    constructor(departments = {}) {
        this.id = departments.id || "";
        this.department = departments.department || "";
        this.description = departments.description || "";       
    }
}

export default department;